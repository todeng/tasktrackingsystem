'use strict';

const config 		= require('./config/gulpConfig')
const gulp		  	= require('gulp');
const nodemon 		= require('gulp-nodemon');
const clean 		= require('gulp-clean');
const inject		= require('gulp-inject');
const browserSync 	= require('browser-sync').create();
const path 			= require('path');
const fs 			= require('fs');
const less 			= require('gulp-less');

gulp.task('default', ['browser-sync']);

gulp.task('browser-sync', ['nodemon'], function() {
	return browserSync.init(null, {
		proxy: `http://localhost:${config.serverListenPort}`,
		files: [path.join(config.paths.distRoot, '**/*.*')],
		browser: 'google chrome',
		port: config.browserSyncListenPort
	});
});

gulp.task('nodemon', ['build'], function (cb) {

	var started = false;

	return nodemon({
		script: 'server/server.js',
		ignore: [config.paths.entryPage],
		watch: [
			path.join(config.paths.clientSrcRoot, '**/*.*'),
			path.join(config.paths.serverSrcRoot, '**/*.*')
		],
		tasks: function (changedFiles) {
			var tasks = []
			changedFiles.forEach(function (file) {

				if (path.extname(file) === '.js' && !~tasks.indexOf('copy:scripts'))
					tasks.push('copy:scripts');

				if (path.extname(file) === '.css' && !~tasks.indexOf('copy:css'))
					tasks.push('copy:css');

				if (path.extname(file) === '.html' && !~tasks.indexOf('copy:html'))
					tasks.push('copy:html');

				if (path.extname(file) === '.less' && !~tasks.indexOf('copy:less'))
					tasks.push('copy:less');
			})

			return tasks;
		}
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		if (!started) {
			cb();
			started = true;
		}
	});
});

gulp.task('copy:assets', function () {
	return gulp.src('**/*.*', {cwd: config.paths.clientAssets})
		.pipe(gulp.dest(config.paths.distRoot));
});

gulp.task('copy:less', function () {
	return gulp.src(config.filter_paths.less, {cwd: config.paths.clientSrcRoot})
		.pipe(less())
		.pipe(gulp.dest(config.paths.cssDist));
});

gulp.task('copy:html', function(){
	return gulp.src(config.filter_paths.templates, {cwd: config.paths.clientSrcRoot})
 		.pipe(gulp.dest(config.paths.distRoot));
})

gulp.task('copy:entryPage', function(){
	return gulp.src(config.filter_paths.entryPage, {cwd: config.paths.clientSrcRoot})
 		.pipe(gulp.dest(config.paths.distRoot));
})

gulp.task('copy:css', function(){
	return gulp.src(config.filter_paths.css, {cwd: config.paths.clientSrcRoot})
 		.pipe(gulp.dest(config.paths.distRoot));
})

gulp.task('copy:scripts', function(){
	return gulp.src(config.filter_paths.js, {cwd: config.paths.clientSrcRoot})
 		.pipe(gulp.dest(config.paths.distRoot));
})

gulp.task('copy:vendors', function(){
	var tasks = [];

	for(var vendorName in config.paths.vendors){
		const 	vendor = config.paths.vendors[vendorName],
				js = vendor.js,
				css = vendor.css,
				fonts = vendor.fonts;
		if(js){
			tasks.push(gulp.src(js)
 				.pipe(gulp.dest(config.paths.vendorsDist)));
		}

		if(css){
			tasks.push(gulp.src(css)
 				.pipe(gulp.dest(config.paths.vendorsDist)));
		}

		if(fonts){
			tasks.push(gulp.src(fonts)
 				.pipe(gulp.dest(config.paths.fontsDist)));
		}
	}

	return tasks;
})

gulp.task('copy', ['copy:less', 'copy:html', 'copy:css', 'copy:scripts', 'copy:vendors', 'copy:assets', 'copy:entryPage']);

gulp.task('inject', ['copy'], function(){
	var target = gulp.src(config.paths.entryPage);
	var sources = gulp.src(config.injections, {read: false, cwd: config.paths.distRoot});
	return target.pipe(inject(sources))
		.pipe(gulp.dest(config.paths.distRoot));
})

gulp.task('build', ['inject']);

gulp.task('clean', function(callback){
	return gulp.src(config.paths.distRoot, {read: false})
		.pipe(clean());
});

gulp.task('install', function(){
	return gulp.src(['./bower.json', './package.json'])
  		.pipe(install());
});
