const router = require('express').Router();
const mongoose = require('mongoose');
const Ticket = mongoose.model('Ticket');

router.get('/', function(req, res) {
	console.log("Ticket get requiest '/'");
	Ticket.find().exec(function(err, tickets){
		if (err) return res.serverError(null, err);
		res.json(tickets);
	})
});

router.get('/:id', function(req, res) {
	Ticket.findOne({ '_id': req.params.id }).exec(function(err, ticket){
		if (err) return res.serverError(null, err);
		res.json(ticket);
	})
});

router.post('/', function(req, res) {
	Ticket.create(req.body, function (err, ticket) {
		if (err) return res.serverError(null, err);
		res.json(ticket);
	})
});

router.put('/:id', function(req, res) {
	Ticket.findOneAndUpdate({ '_id': req.params.id }, req.body, {new: true})
		.exec(function(err, ticket){
			if (err) return res.serverError(null, err);
			res.json(ticket);
		})
});

router.delete('/:id', function(req, res) {
	Ticket.remove({ _id: req.params.id }).exec(function (err) {
		res.json({});
	});
});


module.exports = router;
