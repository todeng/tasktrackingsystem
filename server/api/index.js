var router = require('express').Router();

router.use('/account', require('./account'));
router.use('/auth', require('./auth'));
router.use('/widget', require('./widget'));
router.use('/ticket', require('./ticket'));

module.exports = router;
