const router            = require('express').Router();
const passport          = require('passport');
const mongoose          = require('mongoose');
const cipherService     = require('../services/cipherService')
const User              = mongoose.model('User');
const authorization     = require("./../config/middlewares/authorization");

router.post('/signup', function(req, res) {
  var request_data = {
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    displayName: req.body.displayName
  }

  User.find({email: request_data.email}).exec(function(err, users){
      if(users.lengtn > 0) res.status(500).send("User alredy exist");

      User.create({
        email: request_data.email,
        displayName: request_data.displayName,
        password: request_data.password

      })
      .then(function(user){
          user = user.toJson();

          res.json({
            // TODO: replace with new type of cipher service
            token: cipherService.createToken(user),
            user: user
          });
      });
  })
});

router.post('/signin', function (req, res) {
    passport.authenticate('local', function(err, user, info){
        _onPassportAuth(req, res, err, user, info); 
    })(req, res);
});

router.post('/myinfo', authorization(), function (req, res) {
    var email  = req.user.email;

    User.findOne({email: email}).exec(function(err, user){
        res.json(user.toJson())
    });
});

router.get('/facebook', function(req, res) {
    passport.authenticate('facebook', {
        scope: [ 'email', 'user_about_me']
    })(req, res);
})

router.get('/facebook/callback', function(req, res) {
    passport.authenticate('facebook', function(err, user, info){
        _onPassportAuth(req, res, err, user, info); 
    })(req, res);
})


function _onPassportAuth(req, res, error, user, info) {
    res.json(user);
    return;


    if (error) return res.serverError(error);
    if (!user) return res.unauthorized(info && info.message)
 
    user = user.toJson();    

    return res.json({
    // TODO: replace with new type of cipher service
        token: cipherService.createToken(user),
        user: user
    });
}

module.exports = router;