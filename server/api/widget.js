const router = require('express').Router();
const mongoose = require('mongoose');
const Widget = mongoose.model('Widget');

router.get('/', function(req, res) {
	console.log("Widget get requiest '/'");
	Widget.find().exec(function(err, widgets){
		if (err) return res.serverError(null, err);
		res.json(widgets);
	})
});

router.get('/:id', function(req, res) {
	Widget.findOne({ '_id': req.params.id }).exec(function(err, widget){
		if (err) return res.serverError(null, err);
		res.json(widget);
	})
});

router.post('/', function(req, res) {
	Widget.create(req.body, function (err, widget) {
		if (err) return res.serverError(null, err);
		res.json(widget);
	})
});

router.put('/:id', function(req, res) {
	Widget.findOneAndUpdate({ '_id': req.params.id }, req.body, {new: true})
		.exec(function(err, widget){
			if (err) return res.serverError(null, err);
			res.json(widget);
		})
});

router.delete('/:id', function(req, res) {
	Widget.remove({ _id: req.params.id }).exec(function (err) {
		res.json({});
	});
});

module.exports = router;
