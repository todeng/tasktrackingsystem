const router = require('express').Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const authorization = require("./../../config/middlewares/authorization");


router.get('/', function(req, res) {
	User.find().exec(function(err, users){
		res.json(users);
	})
});

router.get('/:id', function(req, res) {
  res.json({ id: req.params.id });
});

router.post('/', function(req, res) {
	User.create({ email: 'todeng.by@gmail.com' }, function (err, user) {
  		res.json(user);
	})
});

module.exports = router;