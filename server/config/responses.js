const fs = require('fs');
const path = require('path')
const join = path.join;
const models = join(__dirname, './responses');

module.exports = function(epress) {	
	var response = epress.response;

	console.log("Init responses");
	fs.readdirSync(models)
	  	.filter(file => ~file.search(/^[^\.].*\.js$/))
	  	.forEach(file => response[path.basename(file, '.js')] = require(join(models, file)));

	console.log("Init responses finished");
}