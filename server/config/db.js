module.exports = function(mongoose, connectionStr){
	return new Promise((resolve, reject) => {
		connect()
		  	.on('error', console.log)
		  	.on('disconnected', connect)
		  	.once('open', resolve);
	});

	function connect () {
	  var options = { server: { socketOptions: { keepAlive: 1 } } };
	  mongoose.connect(connectionStr, options);

	  return mongoose.connection;
	}
}