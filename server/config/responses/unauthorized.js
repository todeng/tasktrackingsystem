module.exports = function(message){
	var res = this;

	res.format({
	  	'text/plain': function(){
	    	res.send(401, message || 'Unauthorized');
	  	},

	  	'application/json': function(){
	    	res.send(401, { message: message || 'Unauthorized' });
	  	}
	})	
}