module.exports = function(message, data){
	var res = this;

	res.send(500, { message: message || 'Server Error', data: data });
}
