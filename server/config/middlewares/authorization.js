'use strict';

const passport = require('passport');

module.exports = RequireAuthorization;


function RequireAuthorization(roles){

	return function (req, res, next) {
		passport.authenticate('jwt', function (error, user, info) {
	        if (error) return res.serverError(error);
	      	if (!user) 
	       		return res.unauthorized();
	     	req.user = user;
	     	req.isAuthenticated = true;
	 
	     	next();
	    })(req, res);  	
	}
}