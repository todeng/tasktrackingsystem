var jwtStrategy = require('./passport/jwtStrategy.js');
var localStrategy = require('./passport/localStrategy.js');
var facebookStrategy = require('./passport/facebookStrategy.js');

module.exports = function(app, passport){
	app.use(passport.initialize())
	app.use(passport.session());

  	passport.use(localStrategy);
  	passport.use(jwtStrategy);
  	passport.use(facebookStrategy);
}
