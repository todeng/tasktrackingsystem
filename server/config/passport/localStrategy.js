const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cipherService = require("../../services/cipherService")
const mongoose = require('mongoose');
const User              = mongoose.model('User');

var CONFIG = {
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: false
};

function _onLocalStrategyAuth(email, password, next) {
  User.findOne({email: email})
    .exec(function (error, user) {
      if (error) return next(error, false, {});
 
      if (!user) return next(null, false, {
        code: 'E_USER_NOT_FOUND',
        message: email + ' is not found'
      });
 
      if (!cipherService.compareHash(password, user.pass_hash))
        return next(null, false, {
          code: 'E_WRONG_PASSWORD',
          message: 'Password is wrong'
        });
 
      return next(null, user, {});
    });
}

module.exports = new LocalStrategy(CONFIG, _onLocalStrategyAuth);
