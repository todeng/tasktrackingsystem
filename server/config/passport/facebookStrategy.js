const passport 			= require('passport');
const FacebookStrategy 	= require('passport-facebook').Strategy;
const config         	= require('../../../config/serverConfig');
const mongoose 			= require('mongoose');
const User              = mongoose.model('User');


var CONFIG = {
  scope: ['email', 'public_profile'],
  passwordField: 'password',

  passReqToCallback: false
};

module.exports = new FacebookStrategy({
    clientID: config.facebook.clientID,
    clientSecret: config.facebook.clientSecret,
    callbackURL: config.facebook.callbackURL,
    profileFields:  ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'picture'],
    scope: ['email', 'public_profile'],
  },
  function (accessToken, refreshToken, profile, done) {

    console.log(profile);

    const options = {
      criteria: { 'facebook.id': profile.id }
    };
    User.load(options, function (err, user) {
      if (err) return done(err);
      if (!user) {
        user = new User({
          email: profile.emails[0].value,
          displayName: profile.username,
          password: "none",
          provider: 'facebook'
        });
        user.save(function (err) {
          if (err) console.log(err);
          return done(err, user);
        });
      } else {
        return done(err, user);
      }
    });
  }
);
