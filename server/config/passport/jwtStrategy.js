var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var config = require('../../../config/serverConfig')

var CONFIG = {
  secretOrKey: config.secret,
  issuer : config.issuer,
  audience: config.audience,
  passReqToCallback: false,
  jwtFromRequest: ExtractJwt.fromAuthHeader()
};

function _onJwtStrategyAuth(payload, next) {
  var user = payload.user;
  return next(null, user, {});
}

module.exports = new JwtStrategy(CONFIG, _onJwtStrategyAuth);
