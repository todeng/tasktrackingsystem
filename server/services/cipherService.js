var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var config = require('../../config/serverConfig');

module.exports = {
  secret: config.secret,
  issuer: config.issuer,
  audience: config.audience,

  getHash: function (str) {
    if (str) {
      return bcrypt.hashSync(str);
    }
  },

  compareHash: function(hash, str){
    return bcrypt.compareSync(hash, str);
  },

  createToken: function(user)
  {
    return jwt.sign({
        user: user
      },
      settings.secret,
      {
        algorithm: config.jwtAlgorithm,
        expiresInMinutes: config.jwtTokenExpiresOnMins,
        issuer: config.issuer,
        audience: config.audience
      }
    );
  }
};
