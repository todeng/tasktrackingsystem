var express		= require('express');
var bodyParser 	= require('body-parser')
var mongoose	= require('mongoose')
var app         = express();
var passport 	= require("passport")
var config 	= require("../config/serverConfig.js")

app.use(bodyParser());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

require("./config/models")();
require("./config/passport")(app, passport);
require('./config/responses')(express);

app.use('/api', require('./api'));

app.use(express.static(config.staticRoot));

app.get('*', (req, res, next) => {
	res.sendFile(config.indexPage);
});


require("./config/db")(mongoose, config.db)
	.then(listen);


function listen(){
	app.listen(config.port, () => {
	  	console.log(`Server running at http://localhost:${config.port}/`);
	});
}
