const mongoose = require('mongoose');
const cipherService = require('../services/cipherService');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: { type: String, default: '' },
  displayName: { type: String, default: '' },
  photoUrl: { type: String, default: '' },
  pass_hash: { type: String, default: '' },
  provider: { type: String, default: '' }
});

UserSchema
  .virtual('password')
  .set(function (password) {
    this._password = password;
    this.pass_hash = cipherService.getHash(password);
  })
  .get(function () {
    return this._password;
  });

UserSchema.path('email').validate(function (name) {
  return name.length;
}, 'Email cannot be blank');

UserSchema.path('displayName').validate(function (name) {
  return name.length;
}, 'Display Name cannot be blank');

UserSchema.path('email').validate(function (email, fn) {
  const User = mongoose.model('User');

  if (this.isNew || this.isModified('email')) {
    User.find({ email: email }).exec(function (err, users) {
      fn(!err && users.length === 0);
    });
  } else fn(true);
}, 'Email already exists');

UserSchema.path('pass_hash').validate(function (pass_hash) {
  return pass_hash.length && this._password.length;
}, 'Password cannot be blank');

UserSchema.methods = {
	toJson: function(){
		return {
			email: this.email,
			displayName: this.displayName,
			photoUrl: this.photoUrl
		}
	}
}

mongoose.model('User', UserSchema);