const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WidgetSchema = new Schema({
	widget: { type: String, enum : ['chart-widget','pie-widget', 'list-widget'], default: 'list-widget' },
	width: { type: Number, default: 200 },
	height: { type: Number, default: 200 },
	background: { type: String, default: '' },
	borderWidth: { type: Number, default: 1 },
	borderColor: { type: String, default: '' },
	borderRadius: { type: Number, default: 5 }
}, {
	'toJSON': {
		transform: function(doc, ret, options){
			ret.id = ret._id;
			delete ret.__v;
		}
	}
});

mongoose.model('Widget', WidgetSchema);
