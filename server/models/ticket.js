const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TicketSchema = new Schema({
	title: { type: String, default: '' },
	dueDate: { type: Date, default: new Date() },
	status: {
		type: String,
		enum : ['open','inProgress', 'closed'],
		default: 'open'
	}
}, {
	'toJSON': {
		transform: function(doc, ret, options){
			ret.id = ret._id;
			delete ret.__v;
		}
	}
});

mongoose.model('Ticket', TicketSchema);
