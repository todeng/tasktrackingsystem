define(['knockout', 'knockout.validation'], function(ko){
	var TicketModel = function(title, dueDate, status){
		this.title =  ko.observable(title).extend({ validation: {
			minLength: {
				length: 5
			}
		}});
		this.dueDate = ko.observable(dueDate).extend({ validation: {
			reqiured: true
		}});
		this.status = ko.observable(status).extend({ validation: true });

		this.isValid = ko.computed(function() {
			return this.title.isValid() && this.dueDate.isValid();
		}, this)
	}

	TicketModel.prototype = {
		toObject: function(){
			return {
				title: this.title(),
				dueDate: this.dueDate(),
				status: this.status()
			}
		}
	}

	return TicketModel;
})
