define(['knockout', 'settings', 'knockout.validation'], function(ko, settings){
	var WidgetModel = function(widget, width, height, background, borderWidth, borderColor, borderRadius){
		this.widget = ko.observable(widget).extend({ validation: {
			condition: {
				condition: function(value){
					return settings.ticketStatuses.indexOf('value') !== -1;
				},
				message: "Invalid widget"
			}
		}});
		this.width = ko.observable(width).extend({ validation: true });
		this.height = ko.observable(height).extend({ validation: true });
		this.background = ko.observable(background).extend({ validation: true });
		this.borderWidth = ko.observable(borderWidth).extend({ validation: true });
		this.borderColor = ko.observable(borderColor).extend({ validation: true });
		this.borderRadius = ko.observable(borderRadius).extend({ validation: true });

		this.isValid = ko.computed(function() {
			return this.width.isValid() && this.height.isValid() && this.background.isValid()
				&& this.borderWidth.isValid() && this.borderColor.isValid() && this.borderRadius.isValid();
		}, this)
	}

	WidgetModel.prototype = {
		toObject: function(){
			return {
				widget: this.widget(),
				width: this.width(),
				height: this.height(),
				background: this.background(),
				borderWidth: this.borderWidth(),
				borderColor: this.borderColor(),
				borderRadius: this.borderRadius()
			}
		}
	}

	return WidgetModel;

	function isNumeric(n) {
  		return !isNaN(parseFloat(n)) && isFinite(n);
	}
})
