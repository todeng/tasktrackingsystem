define(function(){
	var Page = function(title, view, model){
		this.title = title,
		this.view = view,
		this.mode = model
	}

	Page.prototype = {
		init: function(){ }
	}

	return Page;
})
