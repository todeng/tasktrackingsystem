define(['EventEmitter'], function(EventEmitter){

	var DISPATCH_EVENT = "DISPATCH_EVENT";

	var Dispatcher = function(){
		this._eventEmitter = new EventEmitter();
	}

	Dispatcher.prototype = {
		register: function(callback, context){
			this._eventEmitter.register(DISPATCH_EVENT, callback, context);
		},

		remove: function(callback){
			this._eventEmitter.remove(DISPATCH_EVENT, callback)
		},

		dispatch: function(data){
			this._eventEmitter.emit(DISPATCH_EVENT, data);
		}
	}

	return new Dispatcher();
})
