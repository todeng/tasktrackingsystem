define(function(){
	var Component = function(name, template, viewModel){
		this.name = name;
		this.template = template;
		this.viewModel = {
			createViewModel: function(params, componentInfo){
				return new viewModel(params, componentInfo)
			}
		}
	}

	return function(name, template, viewModel){
		return function(){
			return new Component(name, template, viewModel);
		}
	}
})
