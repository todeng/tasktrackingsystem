define(function(){
	var EventEmitter = function(){
		this._prefix = 'on_';
		this._listeners = {};
	}

	EventEmitter.prototype = {
		register: function(event_name, callback, bind){
			if(typeof event_name !== 'undefined'
				&& typeof callback !== 'function' ){
					return;
				}

			var event_name = this._prefix + event_name;

			if(!this._listeners[event_name]) {
				this._listeners[event_name] = [];
			}

			this._listeners[event_name].push([bind === null ? this : bind, callback]);
		},

		remove: function(event_name, callback){
			var event_name = this._prefix + event_name;

			if(typeof this._listeners[event_name] !== 'undefined'
				&& typeof callback === 'function' ){

				var listener_index = -1;
				for(var i = 0; i < this._listeners[event_name].length; i++){
					if(this._listeners[event_name][i][1] === callback){
						listener_index = i;
						break;
					}
				}

				if(listener_index !== -1){
					this._listeners[event_name].splice(listener_index, 1);
				}
			}
		},

		emit: function(event_name, data){
			var event_name = this._prefix + event_name;

			if(typeof this._listeners[event_name] != 'undefined') {
				for (var i = 0; i < this._listeners[event_name].length; i++) {
					this._listeners[event_name][i][1].call(this._listeners[event_name][i][0], data, event_name);
				}
			}
		}
	}

	return EventEmitter;
})
