define(['require', 'knockout', 'scripts/core/component'], function(require, ko, Component) {
	var options = {
		'copmonentsRootPath': 'scripts/components',
		'templateFileName': 'template.html',
		'viewModel': 'viewModel.js'
	}

	var ComponentFactory = function ComponentFactory(){
		this.init();
	}

	ComponentFactory.prototype = {
		init: function(){
			this.components = {};
		},

		createComponent: function(name, callback){
			if(!callback || !callback.bind)
				return;

			var self = this;
			if(this.components[name]){
				callback(this.components[name]())
			}

			require([options.copmonentsRootPath + "/" + name + "/" + options.viewModel,
					"text!" + options.copmonentsRootPath + "/" + name + "/" + options.templateFileName], function(model, view) {

				var component = self.components[name] = Component(name, view, model);
				callback(component());
			})
		}
	};

	return new ComponentFactory();
})
