define(['knockout', 'validators'], function(ko, validators){


	ko.extenders.validation = function(target, option) {
		target.isValid = ko.observable(true);
		target.errors = ko.observableArray([]);

		target.validate = function(){
			target.errors([]);

			if(typeof option === 'object'){
				for(var validator_name in option){
					var isValid = true;

					if(validators[validator_name]){
						var validator= validators[validator_name];
						var params = option[validator_name];

						if(!validator.checkFunc(target(), params)){
							target.errors.push(params.message || validator.defaultMessage(target(), params));
							isValid = false;
						}
					}

					target.isValid(isValid);
				}
			}
		}

		target.subscribe(function(newValue) {
			target.validate();
		})

		target.validate.bind(this)();
		return target;
	};
})
