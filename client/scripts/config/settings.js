define(function(){
	return {
		ticketStatuses: ['open', 'inProgress', 'pending', 'closed'],

		ticketStatusLabels: {
			open: "Open",
			inProgress: "In progress",
			pending: "Pending",
			closed: "Closed"
		},

		ticketStatusColors: {
			open: "#F7464A",
			inProgress: "#46BFBD",
			pending: "#5AD3D1",
			closed: "green"
		}
	}
})
