define(['componentFactory'], function(componentFactory){
	function configure(ko, components, cb){
		if(!components || !components.length){
			cb();
			return;
		}

		var comonent_name = components.pop()
		componentFactory.createComponent(comonent_name, function(component){
			ko.components.register(comonent_name, component);
			configure(ko, components, cb);
		})
	}

	return {
		configure: configure
	}
})
