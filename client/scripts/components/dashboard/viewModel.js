define(['underscore', 'knockout', 'stores.ticketStore', 'stores.widgetsStore'], function(_, ko, ticketStore, widgetsStore){
	var Dashboard = function() {
		this.tickets = ko.observableArray(ticketStore.getTickets());
		this.widgets = ko.observableArray(widgetsStore.getWidgets());
		this.init();
	}

	_.extend(Dashboard.prototype, {
		init: function(){
			ticketStore.addChangeListener(this._taskStoreChangeHandler, this);
			widgetsStore.addChangeListener(this._widgetStoreChangeHandler, this);
		},

		dispose: function(){
			ticketStore.removeChangeListener(this._taskStoreChangeHandler);
			widgetsStore.removeChangeListener(this._widgetStoreChangeHandler);
		},

		_taskStoreChangeHandler: function(){
			this.tickets(ticketStore.getTickets());
		},

		_widgetStoreChangeHandler: function(){
			this.widgets(widgetsStore.getWidgets());
		}
	})

	return Dashboard
})
