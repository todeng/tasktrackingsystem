define(['underscore', 'knockout', 'stores.sessionStore', 'constants.sessionProperties', 'actions.sessionActions'], function(_, ko, sessionStore, sessionProperties, sessionActions){
	function NavBar() {
		this.api_context = ko.observable(this.getApiContext());
		this.init();
	}

	_.extend(NavBar.prototype, {
		init: function(){
			sessionStore.addChangeListener(this._sessionStoreChanged, this)
		},
		setLocalContext: function(){
			if(this.api_context() !== 'local'){
				sessionActions.setSession({
					[sessionProperties.API_CONTEXT]: 'local'
				})
			}
		},
		setServerContext: function(){
			if(this.api_context() !== 'server'){
				sessionActions.setSession({
					[sessionProperties.API_CONTEXT]: 'server'
				})
			}
		},
		getApiContext(){
			return sessionStore.getSession()[sessionProperties.API_CONTEXT] || 'local'
		},
		dispose: function(){
			sessionStore.removeChangeListener(this._sessionStoreChanged);
		},
		_sessionStoreChanged: function(){
			this.api_context(this.getApiContext());
		}
	})

	return NavBar;
})
