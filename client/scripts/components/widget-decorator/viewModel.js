define(['underscore', 'actions.widgetActions'], function(_, widgetActions){
	var WidgetDecorator = function(params, componentInfo){
		this.id = params.id;
		this.width = params.width || '200';
		this.height = params.height || '200';
		this.background = params.background  || '#F5F5F5';
		this.borderWidth = params.borderWidth || '1';
		this.borderColor = params.borderColor || '#DDDDDD';
		this.borderRadius = params.borderRadius || '5';
	}

	_.extend(WidgetDecorator.prototype,{
		init: function(){

		},
		remove: function(){
			widgetActions.removeWidget(this.id);
		}
	})

	return WidgetDecorator;
})
