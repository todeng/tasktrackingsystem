define(['underscore', 'knockout', 'jquery', 'chart', 'settings', 'utils.ticketUtil'], function(_, ko, $, Chart, settings, ticketUtil){
	var ChartWidget = function(params, componentInfo){
		this.barChart = null;
		this.init(params, componentInfo);
		this.update(params.tickets());
	}

	_.extend(ChartWidget.prototype, {
		init: function(params, componentInfo){
			var self = this;

			params.tickets = params.tickets || ko.observableArray([]);
			params.tickets.subscribe(function(newValue) {
				self.update(newValue);
			});

			var data = {
				labels: [],
				datasets: [
					{
						strokeColor: "rgba(220,220,220,0.8)",
						highlightFill: "rgba(220,220,220,0.75)",
						highlightStroke: "rgba(220,220,220,1)",
						data: []
					}
				]
			};

			for(var index in settings.ticketStatuses){
				var status = settings.ticketStatuses[index];

				data.labels.push(settings.ticketStatusLabels[status]);
				data.datasets[0].data.push(0);
			}

			var ctx = $('canvas', componentInfo.element)[0].getContext("2d");
			this.barChart = new Chart(ctx).Bar(data, {
				scaleShowGridLines : false,
				showScale: false
			});

			for(var index in settings.ticketStatuses){
				var status = settings.ticketStatuses[index];

				this.barChart.datasets[0].bars[index].fillColor = settings.ticketStatusColors[status] || "#F7464A";
			}

			this.barChart.update();

		},

		update: function(tickets){
			var amountInfo = ticketUtil.getTicketsAmountInfo(tickets);

			var data = [];

			for(var index in settings.ticketStatuses){
				var status = settings.ticketStatuses[index];

				this.barChart.datasets[0].bars[index].value = amountInfo[status] || 0
			}

			this.barChart.update();
		}
	})

	return ChartWidget;
})
