define(['underscore', 'knockout', 'stores.ticketStore', 'actions.ticketActions', 'settings'], function(_, ko, ticketStore, ticketActions, settings){
	var Tickets = function(){
		this.tickets = ko.observableArray(ticketStore.getTickets());
		this.init();
		this.settings = settings;
	}

	_.extend(Tickets.prototype, {
		init: function(){
			ticketStore.addChangeListener(this._onticketStoreChanged, this);
		},
		dispose: function(){
			removeStore.addChangeListener(this._onticketStoreChanged);
		},
		_onticketStoreChanged: function(){
			this.tickets(ticketStore.getTickets());
		},
		statusChanged: function(data){
			if(data.id){
				ticketActions.updateTicket(data.id, data);
			}
		},
		remove: function(data){
			if(data.id){
				ticketActions.removeTicket(data.id);
			}
		}
	})

	return Tickets;
})
