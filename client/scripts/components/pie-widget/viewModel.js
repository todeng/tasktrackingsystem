define(['underscore', 'knockout', 'jquery', 'chart', 'settings', 'utils.ticketUtil'], function(_, ko, $, Chart,  settings, ticketUtil){
	var PieWidget = function(params, componentInfo){
		this.pieChart = null;
		this.segments = {};
		this.init_data = [];

		for(var index in settings.ticketStatuses){
			var status = settings.ticketStatuses[index];

			this.segments[status] = {
				value: 0,
				color: settings.ticketStatusColors[status] || "#F7464A",
				label: settings.ticketStatusLabels[status] || "None"
			}

			this.init_data.push(this.segments[status])
		}

		this.init(params, componentInfo);
		this.update(params.tickets());
	}

	_.extend(PieWidget.prototype, {
		init: function(params, componentInfo){
			var self = this;

			params.tickets = params.tickets || ko.observableArray([]);
			params.tickets.subscribe(function(newValue) {
				self.update(newValue);
			});

			this.ctx = $('canvas', componentInfo.element)[0].getContext("2d");

			this.initChart(this.init_data);
		},

		initChart: function(init_data){
			this.pieChart = new Chart(this.ctx).Pie(init_data, {
				scaleShowGridLines : false,
				showScale: false
			});
		},

		update: function(tickets){
			var amountInfo = ticketUtil.getTicketsAmountInfo(tickets);

			if(this.isAllValuesIsZero()){
				for(var index in settings.ticketStatuses){
					var status = settings.ticketStatuses[index];

					this.init_data[index].value = amountInfo[status] || 0;
				}

				this.initChart(this.init_data);
			} else {

				for(var index in settings.ticketStatuses){
					var status = settings.ticketStatuses[index];

					this.pieChart.segments[index].value = amountInfo[status] || 0;
				}

				this.pieChart.update();
			}
		},

		isAllValuesIsZero: function(){
			for(var i = 0; i < this.pieChart.segments.length; i++){
				if(this.pieChart.segments[i].value){
					return false;
				}
			}

			return true;
		}
	})

	return PieWidget;
})
