define(['underscore', 'knockout', 'jquery', 'settings', 'utils.ticketUtil'], function(_, ko, $, settings, ticketUtil){
	var PieWidget = function(params, componentInfo){
		this.statuses = ko.observableArray();
		this.settings = settings;
		this.init(params, componentInfo);
		this.update(params.tickets());
	}

	_.extend(PieWidget.prototype, {
		init: function(params, componentInfo){
			var self = this;

			params.tickets = params.tickets || ko.observableArray([]);
			params.tickets.subscribe(function(newValue) {
				self.update(newValue);
			});
		},

		update: function(tickets){
			var amount = ticketUtil.getTicketsAmountInfo(tickets);

			var statuses = [];

			for(var index in settings.ticketStatuses){
				var status = settings.ticketStatuses[index];

				statuses.push({
					label: settings.ticketStatusLabels[status] || "None",
					value: amount[status]
				})
			}

			this.statuses(statuses);
		}
	})

	return PieWidget;
})
