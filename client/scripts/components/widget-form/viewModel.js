define(['underscore', 'knockout', 'jquery', 'actions.widgetActions', 'models.WidgetModel', 'sliderValue', 'colorValue'], function(_, ko, $, widgetActions, WidgetModel){
	var WidgetForm = function(params, componentInfo){

		this.model = new WidgetModel();
		this.submited = ko.observable(false);
		this.reset();
	}

	_.extend(WidgetForm.prototype, {
		reset: function(){
			this.model.widget('list-widget')
			this.model.width(200);
			this.model.height(200);
			this.model.background('#F5F5F5');
			this.model.borderWidth(1);
			this.model.borderColor('#DDDDDD');
			this.model.borderRadius(5);
		},
		submit: function() {
			this.submited(true);

			if(this.model.isValid()){
				widgetActions.addWidget(this.model.toObject())
			}
		}
	})

	return WidgetForm;
})
