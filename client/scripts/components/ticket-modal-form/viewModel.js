define(['underscore', 'knockout', 'jquery', 'actions.ticketActions', 'models.TicketModel', 'datepicker'], function(_, ko, $, ticketActions, TicketModel){
	var TicketModalForm = function(){
		this.model = new TicketModel('', new Date(), 'open');
		this.submited = ko.observable(false);
	}

	_.extend(TicketModalForm.prototype, {
		submit: function(data, event) {
			this.submited(true);

			if(this.model.isValid()){
				ticketActions.addTicket(this.model.toObject());
				this.reset();
			} else{
				event.stopPropagation();
			}
		},
		reset: function(){
			this.submited(false);
			this.model.title('');
			this.model.dueDate(new Date());
			this.model.status('open');
		}
	})

	return TicketModalForm;
})
