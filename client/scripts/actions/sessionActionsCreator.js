define(['dispatcher', 'constants.sessionEvents', 'utils.sessionUtil'], function(dispatcher, sessionEvents, sessionUtil){
	return {
		getSession: function(){
			var self = this;

			dispatcher.dispatch({
				type: sessionEvents.GET_SESSION
			})

			sessionUtil.getSession()
				.then(function(session){
					self.getSessionSuccess(session);
				})
				.catch(function(err){
					self.getSessionError(err);
				})

		},

		getSessionSuccess: function(session){
			dispatcher.dispatch({
				type: sessionEvents.GET_SESSION_SUCCESS,
				data: {
					session: session
				}
			})
		},

		getSessionError: function(err){
			dispatcher.dispatch({
				type: sessionEvents.GET_SESSION_ERROR,
				data: {
					err: err
				}
			})
		},

		setSession: function(session){
			var self = this;

			dispatcher.dispatch({
				type: sessionEvents.SET_SESSION,
				data: session
			})

			sessionUtil.setSession(session)
				.then(function(session){
					self.setSessionSuccess(session);
				})
				.catch(function(err){
					self.setSessionError(err);
				})
		},

		setSessionSuccess: function(session){
			dispatcher.dispatch({
				type: sessionEvents.SET_SESSION_SUCCESS,
				data: {
					session: session
				}
			})
		},

		setSessionError: function(err){
			dispatcher.dispatch({
				type: sessionEvents.SET_SESSION_ERROR,
				data: {
					err: err
				}
			})
		}
	}
})
