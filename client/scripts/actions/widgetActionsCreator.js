define(['dispatcher', 'constants.widgetEvents', 'api.taskTracking'], function(dispatcher, widgetEvents, taskTracking){
	return {
		getAllWidgets: function(){
			var self = this;

			dispatcher.dispatch({
				type: widgetEvents.GET_ALL_WIDGETS
			});

			taskTracking.getAllWidgets()
				.then(function(widgets){
					self.getAllWidgetsSuccess(widgets);
				})
				.catch(function(err){
					self.getAllWidgetsError(err);
				})
		},

		getAllWidgetsSuccess: function(widgets){
			dispatcher.dispatch({
				type: widgetEvents.GET_ALL_WIDGETS_SUCCESS,
				data: {
					widgets: widgets
				}
			});

		},

		getAllWidgetsError: function(err){
			dispatcher.dispatch({
				type: widgetEvents.GET_ALL_WIDGETS_ERROR,
				data: {
					err: err
				}
			});
		},

		addWidget: function(widget){
			var self = this;

			dispatcher.dispatch({
				type: widgetEvents.ADD_WIDGET,
				data: {
					widget: widget
				}
			});

			taskTracking.addWidget(widget)
				.then(function(widget){
					self.addWidgetSuccess(widget);
				})
				.catch(function(err){
					self.addWidgetError(err);
				})
		},

		addWidgetSuccess: function(widget){
			dispatcher.dispatch({
				type: widgetEvents.ADD_WIDGET_SUCCESS,
				data: {
					widget: widget
				}
			});

		},

		addWidgetError: function(err){
			dispatcher.dispatch({
				type: widgetEvents.ADD_WIDGET_ERROR,
				data: {
					err: err
				}
			});
		},

		updateWidget: function(id, widget){
			var self = this;

			dispatcher.dispatch({
				type: widgetEvents.UPDATE_WIDGET,
				data: {
					id: id,
					widget: widget
				}
			});

			taskTracking.updateWidget(id, widget)
				.then(function(widget){
					self.updateWidgetSuccess(widget);
				})
				.catch(function(err){
					self.updateWidgetError(err);
				});
		},

		updateWidgetSuccess: function(widget){
			dispatcher.dispatch({
				type: widgetEvents.UPDATE_WIDGET_SUCCESS,
				data: {
					widget: widget
				}
			});
		},

		updateWidgetError: function(err){
			dispatcher.dispatch({
				type: widgetEvents.UPDATE_WIDGET_ERROR,
				data: {
					err: err
				}
			});
		},

		removeWidget: function(id){
			var self = this;

			dispatcher.dispatch({
				type: widgetEvents.REMOVE_WIDGET,
				data: {
					id: id
				}
			});

			taskTracking.removeWidget(id)
				.then(function(){
					self.removeWidgetSuccess(id);
				})
				.catch(function(err){
					self.removeWidgetError(err);
				});
		},

		removeWidgetSuccess: function(id){
			dispatcher.dispatch({
				type: widgetEvents.REMOVE_WIDGET_SUCCESS,
				data: {
					id: id
				}
			});
		},

		removeWidgetError: function(err){
			dispatcher.dispatch({
				type: widgetEvents.REMOVE_WIDGET_ERROR,
				data: {
					err: err
				}
			});
		},
	}
})
