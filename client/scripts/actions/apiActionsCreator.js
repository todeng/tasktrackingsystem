define(['utils.apiUtil', 'dispatcher', 'constants.apiEvents', 'actions.sessionActions', 'constants.sessionProperties', 'actions.sessionActions', 'constants.sessionProperties' ],
	function(apiUtil, dispatcher, apiEvents, sessionActions, sessionProperties, sessionActions, sessionProperties){
		return {
			switchApiContextToLocal: function(){
				this.switchApiContext('local');
			},
			switchApiContextToServer: function(){
				this.switchApiContext('server');
			},
			switchApiContext: function(api_context){
				var self = this;

				dispatcher.dispatch({
					type: apiEvents.SWITCH_API_CONTEXT
				})
				apiUtil.switchApiContext(api_context)
					.then(function(){
						self.switchApiContextSuccess(api_context);
					})
					.catch(function(){
						self.switchApiContextSuccess(api_context);
					});
			},

			switchApiContextSuccess: function(apiContext){
				dispatcher.dispatch({
					type: apiEvents.SWITCH_API_CONTEXT_SUCCESS,
					data: {
						apiContext: apiContext
					}
				})
			},

			switchApiContextError: function(apiContext){
				dispatcher.dispatch({
					type: apiEvents.SWITCH_API_CONTEXT_ERROR,
					data: {
						apiContext: apiContext
					}
				})
			}
		}
	})
