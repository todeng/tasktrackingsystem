define(['dispatcher', 'constants.ticketEvents', 'api.taskTracking'], function(dispatcher, ticketEvents, taskTracking){
	return {
		getAllTickets: function(){
			var self = this;

			dispatcher.dispatch({
				type: ticketEvents.GET_ALL_TICKETS
			});

			taskTracking.getAllTickets()
				.then(function(tasks){
					self.getAllTicketsSuccess(tasks);
				})
				.catch(function(err){
					self.getAllTicketsError(err);
				})
		},

		getAllTicketsSuccess: function(tasks){
			dispatcher.dispatch({
				type: ticketEvents.GET_ALL_TICKETS_SUCCESS,
				data: {
					tasks: tasks
				}
			});

		},

		getAllTicketsError: function(err){
			dispatcher.dispatch({
				type: ticketEvents.GET_ALL_TICKETS_ERROR,
				data: {
					err: err
				}
			});
		},

		addTicket: function(task){
			var self = this;

			dispatcher.dispatch({
				type: ticketEvents.ADD_TICKET,
				data: {
					task: task
				}
			});

			taskTracking.addTicket(task)
				.then(function(task){
					self.addTicketSuccess(task);
				})
				.catch(function(err){
					self.addTicketError(err);
				})
		},

		addTicketSuccess: function(task){
			dispatcher.dispatch({
				type: ticketEvents.ADD_TICKET_SUCCESS,
				data: {
					task: task
				}
			});

		},

		addTicketError: function(err){
			dispatcher.dispatch({
				type: ticketEvents.ADD_TICKET_ERROR,
				data: {
					err: err
				}
			});
		},

		updateTicket: function(id, task){
			var self = this;

			dispatcher.dispatch({
				type: ticketEvents.UPDATE_TICKET,
				data: {
					id: id,
					task: task
				}
			});

			taskTracking.updateTicket(id, task)
				.then(function(task){
					self.updateTicketSuccess(id, task);
				})
				.catch(function(err){
					self.updateTicketError(err);
				});
		},

		updateTicketSuccess: function(id, task){
			dispatcher.dispatch({
				type: ticketEvents.UPDATE_TICKET_SUCCESS,
				data: {
					id: id,
					task: task
				}
			});
		},

		updateTicketError: function(id, task){
			dispatcher.dispatch({
				type: ticketEvents.UPDATE_TICKET_ERROR,
				data: {
					err: err
				}
			});
		},

		removeTicket: function(id){
			var self = this;

			dispatcher.dispatch({
				type: ticketEvents.REMOVE_TICKET,
				data: {
					id: id
				}
			});

			taskTracking.removeTicket(id)
				.then(function(){
					self.removeTicketSuccess(id);
				})
				.catch(function(err){
					self.removeTicketError(err);
				});
		},

		removeTicketSuccess: function(id){
			dispatcher.dispatch({
				type: ticketEvents.REMOVE_TICKET_SUCCESS,
				data: {
					id: id
				}
			});
		},

		removeTicketError: function(err){
			dispatcher.dispatch({
				type: ticketEvents.REMOVE_TICKET_ERROR,
				data: {
					err: err
				}
			});
		},
	}
})
