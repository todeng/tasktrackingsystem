require.config({
	baseUrl: '../',
	shim: {
		datepicker: {
            deps: ["jquery"]
        },
		colorpicker: {
            deps: ["jquery"]
        },
		slider: {
            deps: ["jquery"]
        },
	},
    paths: {
		'jquery': 	'vendor/jquery.min',
		'knockout': 'vendor/knockout',

		'text': 	'vendor/text',
		'underscore': 'vendor/underscore',
		'datepicker': 'vendor/bootstrap-datepicker.min',
		'chart': 'vendor/Chart.min',
		'colorpicker': 'vendor/bootstrap-colorpicker.min',
		'colorValue': 'scripts/bindingHandlers/colorValue',
		'slider':  'vendor/bootstrap-slider',
		'sliderValue': 'scripts/bindingHandlers/sliderValue',

		"settings": 'scripts/config/settings',
		'validators': 'scripts/utils/validators',
		'knockout.validation': 'scripts/extenders/validation',

		'componentFactory': 	'scripts/core/componentFactory',
		'EventEmitter': 	'scripts/core/eventEmitter',
		'dispatcher': 		'scripts/core/dispatcher',

		'bindings.DatePicker': 'scripts/bindingHandlers/datePicker',

		'models.TicketModel': 'scripts/models/ticketModel',
		'models.WidgetModel': 'scripts/models/widgetModel',

		'actions.ticketActions': 'scripts/actions/ticketActionsCreator',
		'actions.widgetActions': 'scripts/actions/widgetActionsCreator',
		'actions.apiActions': 'scripts/actions/apiActionsCreator',
		'actions.sessionActions': 'scripts/actions/sessionActionsCreator',

		'constants.apiEvents': 			'scripts/constants/apiEvents',
		'constants.sessionEvents': 		'scripts/constants/sessionEvents',
		'constants.sessionProperties': 	'scripts/constants/sessionProperties',
		'constants.ticketEvents': 		'scripts/constants/ticketEvents',
		'constants.widgetEvents': 		'scripts/constants/widgetEvents',
		'constants.classificators': 	'scripts/constants/classificators',

		'stores.BaseStore':		'scripts/stores/baseStore',
		'stores.sessionStore':	'scripts/stores/sessionStore',
		'stores.ticketStore':	'scripts/stores/ticketStore',
		'stores.widgetsStore':	'scripts/stores/widgetsStore',

		'utils.sessionUtil': 	'scripts/utils/sessionUtil',
		'utils.apiUtil': 		'scripts/utils/apiUtil',
		'utils.ticketUtil': 	'scripts/utils/ticketUtil',

		'api.TaskTrackingApi': 			'scripts/api/taskTracking/taskTrackingApi',
		'api.TaskTrackingApiBase': 		'scripts/api/taskTracking/taskTrackingApiBase',
		'api.TaskTrackingApiFactory': 	'scripts/api/taskTracking/taskTrackingApiFactory',
		'api.TaskTrackingApiLocal': 	'scripts/api/taskTracking/taskTrackingApiLocal',
		'api.TaskTrackingApiServer': 	'scripts/api/taskTracking/taskTrackingApiServer',
		'api.taskTracking': 			'scripts/api/taskTracking'
	}
});
define(['knockout', 'scripts/config/components', 'bindings.DatePicker'], function(ko, componentsConfig, DatePicker) {


	ko.bindingHandlers.datepicker = new DatePicker();
	InitStores();

	componentsConfig.configure(ko, ['nav-bar', 'page-layout', 'tickets', 'ticket-modal-form', 'chart-widget', 'pie-widget',
			'list-widget', 'dashboard', 'widget-decorator', 'widget-form'], function(){
		ko.applyBindings();
		console.log('RequireJs bootstraped');
	});

	function InitStores(){
		require(['dispatcher', 'constants.apiEvents', 'constants.sessionProperties', 'actions.sessionActions', 'actions.ticketActions', 'actions.widgetActions',
		  'actions.apiActions', 'stores.sessionStore', 'stores.ticketStore', 'stores.widgetsStore'],
			function(dispatcher, apiEvents, sessionProperties, sessionActions, ticketActions, widgetActions, apiActions, sessionStore){
				var api_context = null;
				sessionStore.addChangeListener(function(){
					var session_api_context = sessionStore.getSession()[sessionProperties.API_CONTEXT];

					if(!session_api_context){
						var session = {};
						session[sessionProperties.API_CONTEXT] = 'local';

						sessionActions.setSession(session);
						return;
					}

					if(session_api_context !== api_context){
						api_context = session_api_context;
						apiActions.switchApiContext(session_api_context);
					}
				});
				sessionActions.getSession();

				dispatcher.register(function(event){
					if(event.type === apiEvents.SWITCH_API_CONTEXT_SUCCESS){
						ticketActions.getAllTickets();
						widgetActions.getAllWidgets();
					}
				})
			})
	}
});
