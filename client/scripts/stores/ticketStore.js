define(['underscore', 'stores.BaseStore', 'api.taskTracking', 'constants.ticketEvents', 'constants.apiEvents', 'dispatcher'], function(_, BaseStore, taskTracking, ticketEvents, apiEvents, dispatcher){
	var ticketStore = function(){
		this._tasks = [];

		this.init();
	}

	ticketStore.prototype = new BaseStore();
	_.extend(ticketStore.prototype, {
		init: function(){
			dispatcher.register(this._dispatchHandler, this);
		},
		getTickets: function(){
			return this._tasks;
		},
		_dispatchHandler: function(event){
			switch (event.type) {
				case ticketEvents.GET_ALL_TICKETS_SUCCESS:
					this._tasks = event.data.tasks;
					this.emitChange();
					break;
				case ticketEvents.ADD_TICKET_SUCCESS:
					this._tasks.push(event.data.task);
					this.emitChange();
					break;
				case ticketEvents.GET_ALL_TICKETS_ERROR:
					this._tasks = [];
					this.emitChange();
					break;
				case ticketEvents.UPDATE_TICKET_SUCCESS:
					for(var i in this._tasks){
						if(this._tasks[i].id === event.data.id ){
							this._tasks[i] = event.data.task;
							break;
						}
					}
					this.emitChange();
					break;
				case ticketEvents.REMOVE_TICKET_SUCCESS:
					for(var i in this._tasks){
						if(this._tasks[i].id === event.data.id ){
							this._tasks.splice(i,1);
							break;
						}
					}
					this.emitChange();
					break;
			}
		}
	});

	return new ticketStore();
})
