define(['underscore', 'EventEmitter', 'dispatcher'], function(_, EventEmitter, dispatcher){
	var BaseStore = function(options){
		options = options || {};

		this._changeEventName = options.changeEventName || 'CHANGED';
	}

	BaseStore.prototype = new EventEmitter();
	_.extend(BaseStore.prototype, {
		addChangeListener: function(callback, context){
			this.register(this._changeEventName, callback, context);
		},
		removeChangeListener: function(callback){
			this.remove(this._changeEventName, callback);
		},
		emitChange:  function() {
			this.emit(this._changeEventName);
		}
	});

	return BaseStore;
})
