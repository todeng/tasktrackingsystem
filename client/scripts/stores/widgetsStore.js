define(['underscore', 'stores.BaseStore', 'api.taskTracking', 'constants.widgetEvents', 'constants.apiEvents', 'dispatcher'], function(_, BaseStore, taskTracking, widgetEvents, apiEvents, dispatcher){
	var ticketStore = function(){
		this._widgets = [];

		this.init();
	}

	ticketStore.prototype = new BaseStore();
	_.extend(ticketStore.prototype, {
		init: function(){
			dispatcher.register(this._dispatchHandler, this);
		},
		getWidgets: function(){
			return this._widgets;
		},
		_dispatchHandler: function(event){
			switch (event.type) {
				case widgetEvents.GET_ALL_WIDGETS_SUCCESS:
					this._widgets = event.data.widgets;
					this.emitChange();
					break;
				case widgetEvents.GET_ALL_WIDGETS_ERROR:
					this._widgets = [];
					this.emitChange();
					break;
				case widgetEvents.ADD_WIDGET_SUCCESS:
					this._widgets.push(event.data.widget);
					this.emitChange();
					break;
				case widgetEvents.UPDATE_WIDGET_SUCCESS:
					for(var i in this._widgets){
						if(this._widgets[i].id === event.data.id ){
							this._widgets[i] = event.data.widget;
							break;
						}
					}
					this.emitChange();
					break;
				case widgetEvents.REMOVE_WIDGET_SUCCESS:
					for(var i in this._widgets){
						if(this._widgets[i].id === event.data.id ){
							this._widgets.splice(i,1);
							break;
						}
					}
					this.emitChange();
					break;
			}
		}
	});

	return new ticketStore();
})
