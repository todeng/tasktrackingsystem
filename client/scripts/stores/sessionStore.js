define(['underscore', 'stores.BaseStore', 'constants.sessionEvents', 'dispatcher'], function(_, BaseStore, sessionEvents, dispatcher){
	var SessionStore = function(){
		this._session = {};

		this.init();
	}

	SessionStore.prototype = new BaseStore();
	_.extend(SessionStore.prototype, {
		init(){
			dispatcher.register(this._dispatchHandler, this);
		},
		getSession: function(){
			return this._session;
		},
		_dispatchHandler: function(event){
			switch (event.type) {
				case sessionEvents.SET_SESSION_SUCCESS:
				case sessionEvents.GET_SESSION_SUCCESS:
					this._session = event.data.session;
					this.emitChange();
					break;
			}
		}
	})

	return new SessionStore();
})
