define(['api.TaskTrackingApiFactory', 'api.TaskTrackingApi'], function(TaskTrackingApiFactory, TaskTrackingApi){
	var api = new TaskTrackingApi(new TaskTrackingApiFactory().createTaskTrackingApi('local'))

	return api;
})
