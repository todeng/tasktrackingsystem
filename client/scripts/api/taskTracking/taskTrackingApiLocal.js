define(['underscore', 'api.TaskTrackingApiBase', 'underscore'], function(_, TaskTrackingApiBase, _){
	var TICKETS_DOCUMENT_KEY = "tasks";
	var WIDGETS_DOCUMENT_KEY = "widgets";

	var TaskTrackingApiLocal = function(){ }

	TaskTrackingApiLocal.prototype = new TaskTrackingApiBase();

	_.extend(TaskTrackingApiLocal.prototype, {
		_getAll: function(key){
			return new Promise(function(resolve, reject) {
				var widgets = localStorage.getItem(key);

				if(!widgets){
					widgets = [];
					localStorage.setItem(key, JSON.stringify(widgets));
				} else{
					widgets = JSON.parse(widgets)
				}

				resolve(widgets);
			})
		},
		_get: function(key, id){
			var self = this;

			return new Promise(function(resolve, reject) {
				self._getAll(key).then(function(widgets){
					resolve(_.first(_.where(widgets, {id: id})));
				})
			})
		},
		_add: function(key, widget){
			widget = _.clone(widget);
			var self = this;

			return new Promise(function(resolve, reject) {
				self._getAll(key).then(function(widgets){
					var new_id = 1;
					if(widgets.length > 0){
						new_id = _.max(widgets, function(widget){ return widget.id; }).id + 1;
					}
					widget.id = new_id;
					widgets.push(widget);

					localStorage.setItem(key, JSON.stringify(widgets));

					resolve(widget);
				})
			})
		},
		_remove: function(key, id){
			var self = this;

			return new Promise(function(resolve, reject) {
				self._getAll(key).then(function(widgets){
					widgets = _.reject(widgets, function(widget){ return widget.id === id});
					localStorage.setItem(key, JSON.stringify(widgets));

					resolve();
				})
			})
		},
		_update: function(key, id, widget){
			widget = _.clone(widget);
			widget.id = id;
			var self = this;
			return new Promise(function(resolve, reject) {
				self._getAll(key).then(function(widgets){
					var local_widget = _.first(_.where(widgets, {id: id}));

					if(local_widget){
						local_widget = _.extend(local_widget, widget);
						localStorage.setItem(key, JSON.stringify(widgets));
					}

					resolve(local_widget);
				})
			})
		},

		getAllWidgets: function(){
			return this._getAll(WIDGETS_DOCUMENT_KEY);
		},
		getWidget: function(id){
			return this._get(WIDGETS_DOCUMENT_KEY, id);
		},
		addWidget: function(widget){
			return this._add(WIDGETS_DOCUMENT_KEY, widget);
		},
		removeWidget: function(id){
			return this._remove(WIDGETS_DOCUMENT_KEY, id);
		},
		updateWidget: function(id, widget){
			return this._update(WIDGETS_DOCUMENT_KEY, id, widget);
		},

		getAllTickets: function(){
			return this._getAll(TICKETS_DOCUMENT_KEY);
		},
		getTicket: function(id){
			return this._get(TICKETS_DOCUMENT_KEY, id);
		},
		addTicket: function(task){
			return this._add(TICKETS_DOCUMENT_KEY, task);
		},
		removeTicket: function(id){
			return this._remove(TICKETS_DOCUMENT_KEY, id);
		},
		updateTicket: function(id, task){
			return this._update(TICKETS_DOCUMENT_KEY, id, task);
		}
	});

	return TaskTrackingApiLocal;
})
