define(['underscore'], function(_){
	var TaskTrackingApiBase = function(){ }

	_.extend(TaskTrackingApiBase.prototype, {
		getAllWidgets: function(){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		getWidget: function(id){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		addWidget: function(widget){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		updateWidget: function(id, widget){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		removeWidget: function(id){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},

		getAllTickets: function(){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		getTicket: function(id){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		addTicket: function(task){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		updateTicket: function(id, task){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })},
		removeTicket: function(id){ return new Promise(function(resolve, reject) { throw new Error("Not implemented"); })}
	});

	return TaskTrackingApiBase;
})
