define(['underscore', 'api.TaskTrackingApiBase'],	function(_, TaskTrackingApiBase){

	var TaskTrackingApi = function(apiImpl){
		this._apiImpl = apiImpl;

		this.setApiImplementation = function(apiImpl){
			this._apiImpl = apiImpl;
		}
	}

	TaskTrackingApi.prototype = new TaskTrackingApiBase();
	_.extend(TaskTrackingApi.prototype, {
		getAllWidgets: function(){ return this._apiImpl.getAllWidgets();},
		getWidget: function(id){ return this._apiImpl.getWidget(id);},
		addWidget: function(widget){ return this._apiImpl.addWidget(widget);},
		removeWidget: function(id){ return this._apiImpl.removeWidget(id);},
		updateWidget: function(id, widget){ return this._apiImpl.updateWidget(id, widget);},

		getAllTickets: function(){ return this._apiImpl.getAllTickets(); },
		getTicket: function(id){ return this._apiImpl.getTicket(id); },
		addTicket: function(task){ return this._apiImpl.addTicket(task); },
		removeTicket: function(id){ return this._apiImpl.removeTicket(id); },
		updateTicket: function(id, task){ return this._apiImpl.updateTicket(id, task); }
	})

	return TaskTrackingApi;
})
