define(['underscore', 'api.TaskTrackingApiLocal', 'api.TaskTrackingApiServer'], function(_, TaskTrackingApiLocal, TaskTrackingApiServer){
	var DEFAULT_API_CONTEXT = 'local';

	var TaskTrackingApiFactory = function(){
		this._builders = {
			'local': function() { return new TaskTrackingApiLocal(); },
			'server': function() { return new TaskTrackingApiServer(); }
		}
	}

	_.extend(TaskTrackingApiFactory.prototype, {
		'createTaskTrackingApi': function(context){
			if(typeof this._builders[context] === 'function'){
				return this._builders[context]();
			}

			return this._builders[DEFAULT_API_CONTEXT]();
		}
	})

	return TaskTrackingApiFactory;
})
