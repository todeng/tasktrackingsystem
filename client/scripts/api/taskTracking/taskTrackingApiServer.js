define(['underscore', 'api.TaskTrackingApiBase'], function(_, TaskTrackingApiBase){

	var models = {
		ticket: 'ticket',
		widget: 'widget'
	}

	var TaskTrackingApiServer = function(){ }

	TaskTrackingApiServer.prototype = new TaskTrackingApiBase();

	_.extend(TaskTrackingApiServer.prototype, {
		_getAll: function(key){
			var settings = {
				"url": "/api/" + key,
				"method": "GET",
				"headers": {
					"content-type": "application/json",
				}
			}

			return new Promise(function(resolve, reject) {
				$.ajax(settings).done(function (data) {
					resolve(data)
				});
			})
		},
		_get: function(key, id){
			var settings = {
				"url": "/api/" + key + '/' + id,
				"method": "GET",
				"headers": {
					"content-type": "application/json",
				}
			}

			return new Promise(function(resolve, reject) {
				$.ajax(settings).done(function (data) {
					resolve(data)
				});
			})
		},
		_add: function(key, data){
			var settings = {
				"url": "/api/" + key,
				"method": "POST",
				"headers": {
					"content-type": "application/json",
				},
				"data": JSON.stringify(data)
			}

			return new Promise(function(resolve, reject) {
				$.ajax(settings).done(function (data) {
					resolve(data)
				});
			})
		},
		_remove: function(key, id){
			var settings = {
				"url": "/api/" + key + "/" + id,
				"method": "DELETE",
				"headers": {
					"content-type": "application/json",
				}
			}

			return new Promise(function(resolve, reject) {
				$.ajax(settings).done(function (data) {
					resolve(data)
				});
			})
		},
		_update: function(key, id, data){
			var settings = {
				"url": "/api/" + key + "/" + id,
				"method": "PUT",
				"headers": {
					"content-type": "application/json",
				},
				"data": JSON.stringify(data)
			}

			return new Promise(function(resolve, reject) {
				$.ajax(settings).done(function (data) {
					resolve(data)
				});
			})
		},

		getAllWidgets: function(){
			return this._getAll(models.widget);
		},
		getWidget: function(id){
			return this._get(models.widget, id);
		},
		addWidget: function(widget){
			return this._add(models.widget, widget);
		},
		removeWidget: function(id){
			return this._remove(models.widget, id);
		},
		updateWidget: function(id, widget){
			return this._update(models.widget, id, widget);
		},

		getAllTickets: function(){
			return this._getAll(models.ticket);
		},
		getTicket: function(id){
			return this._get(models.ticket, id);
		},
		addTicket: function(task){
			return this._add(models.ticket, task);
		},
		removeTicket: function(id){
			return this._remove(models.ticket, id);
		},
		updateTicket: function(id, task){
			return this._update(models.ticket, id, task);
		}
	});

	return TaskTrackingApiServer;
})
