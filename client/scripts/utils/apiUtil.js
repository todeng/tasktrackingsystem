define(['api.taskTracking', 'api.TaskTrackingApiFactory'], function(taskTracking, TaskTrackingApiFactory){
	return {
		switchApiContext: function(api_context){
			return new Promise(function(resolve, reject){
				var factory = new TaskTrackingApiFactory();

				taskTracking.setApiImplementation(factory.createTaskTrackingApi(api_context));
				resolve(api_context);
			})
		}
	}
})
