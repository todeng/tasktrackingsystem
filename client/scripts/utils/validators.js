window.validators = (function ()
{
	return {
		required: {
			checkFunc: function(value, params){
				return !!value;
			},
			defaultMessage: function(value, params) {
				return `Field is required`
			}
		},
		isNumber: {
			checkFunc: function(value, params){
				var re = /^\d+($|.\d+$)/;
				return re.test(value);
			},
			defaultMessage: function(value, params) {
				return `Field is not number`
			}
		},
		email: {
			checkFunc: function(value, params){
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(value);
			},
			defaultMessage: function(value, params) {
				return `Invalid email`
			}
		},
		inRange: {
			checkFunc: function(value, params){
				return window.validators.isNumber.checkFunc(value) && params.min < +value && params.max > +value;
			},
			defaultMessage: function(value, params) {
				return `Field must be range between ${params.min} and ${params.max}`
			}
		},

		minLength: {
			checkFunc: function(value, params){
				return typeof value === 'string' && value.length > params.length
			},
			defaultMessage: function(value, params) {
				return `Title length must not less when ${params.length} chars`
			}
		},

		condition: {
			checkFunc: function(value, params){
				return typeof params.condition === 'function' && params.condition()
			},
			defaultMessage: function(value, params) {
				return `Condition returned false`
			}
		}
	};
}());
if(typeof define === 'function' ){
	define(window.validators);
}
