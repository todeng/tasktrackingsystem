define(function(){
	var SESSION_LOCAL_STORAGE_KEY = "session";

	return {
		getSession: function(){
			return new Promise(function(resolve, reject){
				if(localStorage && localStorage.getItem){
					var json_session = localStorage.getItem(SESSION_LOCAL_STORAGE_KEY) || '{}';

					try{
						var session = JSON.parse(json_session);
						resolve(session);
					}
					catch(e){
						var session = {};
						localStorage.setItem(SESSION_LOCAL_STORAGE_KEY, JSON.stringify(session))
						resolve(session);
					}
				}
			})
		},

		setSession: function(new_session_values){
			var self = this;

			return new Promise(function(resolve, reject){
				if(localStorage && localStorage.getItem){
					self.getSession()
						.then(function(session){
							session = Object.assign(session, new_session_values);
							localStorage.setItem(SESSION_LOCAL_STORAGE_KEY, JSON.stringify(session));
							resolve(session);
						})
				}
			})
		}
	}
})
