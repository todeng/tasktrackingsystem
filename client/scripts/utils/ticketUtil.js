define(['settings'], function(settings){
	return {
		getTicketsAmountInfo(tickets){
			var counter = { };

			for(var index in settings.ticketStatuses){
				var status = settings.ticketStatuses[index]

				counter[status] = 0;
			}

			for(var i = 0; i < tickets.length; i++){
				counter[tickets[i].status]++;
			}

			return counter;
		}
	}
})
