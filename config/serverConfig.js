var path = require('path');

module.exports = {
	jwtTokenExpiresOnMins: 60 * 24,
	secret: process.env.tokenSecret || "4ukI0uIVnB3iIhfhghfjO41EAtl2550J5F7Trtwe7OM",
	jwtAlgorithm: "HS256",
	issuer: "localhost",
	audience: "localhost",

	db: 'mongodb://localhost/noobjs_dev',

	staticRoot: path.join(__dirname, '../www'),
	indexPage: path.join(__dirname, '../www/index.html'),
	port: process.env.PORT || 8082,

	facebook: {
		clientID: process.env.FACEBOOK_CLIENTID || "822762271168936",
		clientSecret: process.env.FACEBOOK_SECRET || "a4b8c00fe76433e1270fd98b90da6261",
		callbackURL: 'http://localhost:7000/api/auth/facebook/callback'
  	}
}
