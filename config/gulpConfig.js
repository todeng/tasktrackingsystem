const path 			= require('path');
const serverConfig	= require('./serverConfig.js');

module.exports = {};

module.exports.serverListenPort 		= serverConfig.port;
module.exports.browserSyncListenPort 	= 7000;

const uri_paths 		= module.exports.paths = {};
uri_paths.distRoot 		= path.join(__dirname, '../www');
uri_paths.serverSrcRoot = path.join(__dirname, '../server');
uri_paths.clientSrcRoot = path.join(__dirname, '../client');
uri_paths.clientAssets 	= path.join(uri_paths.clientSrcRoot, 'assets');
uri_paths.entryPage 	= path.join(uri_paths.distRoot, 'index.html');
uri_paths.scriptsDist 	= path.join(uri_paths.distRoot, 'scripts');
uri_paths.cssDist 		= path.join(uri_paths.distRoot, 'css');
uri_paths.fontsDist 	= path.join(uri_paths.distRoot, 'fonts');
uri_paths.vendorsDist 	= path.join(uri_paths.distRoot, 'vendor');

const filter_paths 		= module.exports.filter_paths = {};
filter_paths.entryPage 	= 'index.html';
filter_paths.templates	= ['**/*.html', '!index.html'];
filter_paths.js 		= ['**/*.js'];
filter_paths.css 		= ['**/*.css'];
filter_paths.less 		= ['**/*.less'];

uri_paths.vendors = {
	'bootstrap': {
		fonts: 	[path.join(__dirname, '../bower_components/bootstrap/dist/fonts/*.*')],
		css: 	[path.join(__dirname, '../bower_components/bootstrap/dist/css/bootstrap.min.css')],
		js: 	[path.join(__dirname, '../bower_components/bootstrap/dist/js/bootstrap.min.js')]
	},
	'bootstrap-datepicker': {
		css: 	[path.join(__dirname, '../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')],
		js: 	[path.join(__dirname, '../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')]
	},
	'jquery': {
		js: 	[path.join(__dirname, '../bower_components/jquery/dist/jquery.min.js')]
	},
	'font-awesome': {
		fonts: 	[path.join(__dirname, '../bower_components/font-awesome/fonts/*.*')],
		css: 	[path.join(__dirname, '../bower_components/font-awesome/css/font-awesome.min.css')]
	},
	'knockout': {
		js: 	[path.join(__dirname, '../bower_components/knockout/dist/knockout.js')]
	},
	'knockout-validation': {
		js: 	[path.join(__dirname, '../bower_components/knockout-validation/dist/knockout.validation.min.js')]
	},
	'requirejs': {
		js: 	[path.join(__dirname, '../bower_components/requirejs/require.js')]
	},
	'text': {
		js: 	[path.join(__dirname, '../bower_components/text/text.js')]
	},
	'underscore': {
		js: 	[path.join(__dirname, '../bower_components/underscore/underscore.js')]
	},
	'Chart.js': {
		js: 	[path.join(__dirname, '../bower_components/Chart.js/Chart.min.js')]
	},
	'mjolnic-bootstrap-colorpicker': {
		js: 	[path.join(__dirname, '../bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')],
		css: 	[path.join(__dirname, '../bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')],
	},
	'bootstrap-slider': {
		js: 	[path.join(__dirname, '../bower_components/bootstrap-slider/bootstrap-slider.js')],
		css: 	[path.join(__dirname, '../bower_components/bootstrap-slider/slider.css')],
	}
}

module.exports.injections = [
	path.join(uri_paths.vendorsDist, '**/*.css'),
	path.join(uri_paths.cssDist, '**/*.css'),
	path.join(uri_paths.vendorsDist, 'jquery.min.js'),
	path.join(uri_paths.vendorsDist, 'knockout.js'),
	path.join(uri_paths.vendorsDist, 'bootstrap.min.js')
]
